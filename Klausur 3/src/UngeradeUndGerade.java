import java.util.Scanner;
public class UngeradeUndGerade {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		int gerade = 0;
		int ungerade = 0;
		
		System.out.println("Das Programm gibt alle ungerade Zahlen innerhalb eines Intervalls aus.");
		
		System.out.println("Geben Sie bitte die erste Zahl ein: ");
		int zahl1 = tastatur.nextInt();
		System.out.println("Geben Sie bitte die zweite Zahl ein: ");
		int zahl2 = tastatur.nextInt();
		
		if (zahl1 == zahl2) {
			System.out.println("\nDie beiden Zahlen sind gleich gro�");
		}
		else {
			if (zahl1 > zahl2) {
				
				int temp = zahl1;
				zahl1 = zahl2;
				zahl2 = temp;
				System.out.println("Die Zahlen wurden getauscht.\n");
			}
			System.out.println("\nDie ungeraden Zahlen zwischen den eingegebenen Zahlen sind: \n");
			for (int i = zahl1; i <= zahl2; i++) {
				
				if (i % 2 == 0) {
					
					gerade += 1;	
					
				}
				else {
					
					System.out.println(i);
					ungerade += 1;
					
				}
			}
			System.out.println("\nDie Anzahl der geraden Zahlen ist: " + gerade);
			System.out.println("\nDie Anzahl der ungeraden Zahlen ist: " + ungerade);
		}	
		tastatur.close();
	}

}
