
public class Captain {

	private String surname;
	private String name;
	private int captainYears;
	private double gehalt;
	

	//Konstruktoren Anfang
	// TODO: 4. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen
	// initialisiert.
	public Captain (String surname, String name) {
		this.surname = surname;
		this.name = name;
	}
	
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.
	public Captain (String name, String surname, int captainYears, double salary) {
		this.name = name;
		this.surname = surname;
		this.setCaptainYears(captainYears);
		this.gehalt = salary;
	}
	//Konstruktoren Ende
	
	//set-Get Anfang
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}
	
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	
	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode für
	// captainYears.
	// Berücksichtigen Sie, dass das Jahr nach Christus sein soll.
	public void setCaptainYears(int captainYears) {
		if (captainYears>0)
			this.captainYears = captainYears;
	}
	
	public int getCaptainYears() {
		return captainYears;
	}

	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Berücksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		if (gehalt>0)
			this.gehalt = gehalt;
	}

	public double getSalary() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methoden.
		return gehalt;
	}
	//set-get Ende
	
	// TODO: 7. Implementieren Sie eine Methode die Vor- und Nachnamen ausgibt.
	public String vollname () {
		String vollname = surname + " " + name;
		return vollname;
	}

	public String toString() {
		return "Captain {surname = " + surname + ", name = " + name + ", captainyears = " + captainYears + ", salary = " + gehalt + "}";
	}
}