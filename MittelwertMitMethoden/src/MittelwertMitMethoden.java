import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		double m;
		int anzahlWerte;
		
		Scanner myScanner = new Scanner(System.in);
		programmhinweis("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		anzahlWerte = eingabeAnzahl(myScanner, "Geben Sie die Anzahl der einzugebenden Zahlen: ");

		int[] intList = new int[anzahlWerte];
		
		intList = einlesenInArray(myScanner, intList);
		
		m = mittelwertBerechnung(intList);
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static void programmhinweis(String text) {
		System.out.println(text);
	}
	
	public static int eingabeAnzahl(Scanner ms, String text ) {
		System.out.print(text);
		int zahl = ms.nextInt();
		return zahl;
	}
	
	public static int[] einlesenInArray(Scanner ms, int[] intList) {
		for (int i = 0; i < intList.length; i++) {
			System.out.println("Bitte geben Sie eine Zahl ein: ");
			intList[i] = ms.nextInt();
		}
		return intList;
	}
	
	public static double mittelwertBerechnung(int[] intList) {
		double summe = 0;
		for (int tmp : intList) {
			summe += tmp;
		}
		double m = summe / intList.length;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}
