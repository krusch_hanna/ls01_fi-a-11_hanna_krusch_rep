import java.util.Scanner;

public class Volumen {

	public static void main(String[] args) {
		 Scanner myScanner = new Scanner(System.in);
		double v;
		double r;
		
		System.out.print(" Der Radius: ");
        r = myScanner.nextDouble();
		
		v = (4.0 * Math.PI * r * r * r) / 3.0;
		System.out.print(" Das Volumen betr�gt " + v);
	}

}
