import java.util.Scanner;

public class AufgabeAuslagern {

    public static void main(String[] args) {
       
        Scanner myScanner = new Scanner(System.in);
       
        //1.Programmhinweis
        Programmhinweis();
        
        //4.Eingabe
        double zahl1 = EingabeZahl1(myScanner);
        double zahl2 = EingabeZahl2(myScanner);

        //3.Verarbeitung
        double erg = Verarbeitung(zahl1, zahl2);

        //2.Ausgabe   
        Ausgabe(zahl1, zahl2, erg);
        
        myScanner.close();
        
    }
    
    public static void Programmhinweis() {
    	System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
    }
    
    public static double EingabeZahl1(Scanner myScanner) {
    	System.out.printf(" 1. Zahl: ");
        double zahl1 = myScanner.nextDouble();
        return zahl1;
    }
    
    public static double EingabeZahl2(Scanner myScanner) {
        System.out.printf(" 2. Zahl: ");
        double zahl2 = myScanner.nextDouble();
        return zahl2;
        //Ich musste die zweite Zahl in eine Extra Methode auslagern, 
        //weil das Programm keine zwei returns innerhalb einer Methode wollte
        //Ich hoffe, dass das nicht schlimm ist. :c
    }
    
    public static double Verarbeitung(double zahl1, double zahl2) {
    	double erg = zahl1 * zahl2;
    	return erg;
    }
    
    public static void Ausgabe(double zahl1, double zahl2, double erg) {
    	System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", zahl1, zahl2, erg);
    }
}
