
public class Konsolenausgabe2 {

	public static void main(String[] args) {
		//Aufgabe3
		//Alle Werte
		String f = "Fahrenheit";
		String c = "Celsius";
		double e = -20;
		double ee = -28.8889;
		double z = -10;
		double zz = -23.3333;
		double d = +0;
		double dd = -17.7778;
		double v = +20;
		double vv = -6.6667;
		double l = +30;
		double ll = -1.1111;
		
		//Tabellenkopf ausgeben 
		System.out.printf( "%-12s|", f);
		System.out.printf( "%10s\n", c);
		
		System.out.printf("------------|----------\n");
		
		//Werte formatieren und ausgeben
		System.out.printf( "%-12.0f|", e);
		System.out.printf( "%10.2f\n", ee);
		
		System.out.printf( "%-12.0f|", z);
		System.out.printf( "%10.2f\n", zz);
		
		System.out.printf( "%+-12.0f|", d);
		System.out.printf( "%10.2f\n", dd);
		
		System.out.printf( "%+-12.0f|", v);
		System.out.printf( "%10.2f\n", vv);
		
		System.out.printf( "%+-12.0f|", l);
		System.out.printf( "%10.2f\n", ll);
		
		
	}

}
