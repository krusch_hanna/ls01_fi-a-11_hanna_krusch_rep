import java.util.ArrayList;

public class Raumschiff {
	//Attribute
	private int photonentorpedoAnzahl;
	private double energieversorgungInProzent;
	private double schildeInProzent;
	private double huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	static private ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktoren
	public Raumschiff() {}
	
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	public int getPhotonentorpedoAnzahl() {return photonentorpedoAnzahl;}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;}
	
	public int getEnergieversorgungInProzent() {return energieversorgungInProzent;}
	
	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {this.energieversorgungInProzent = energieversorgungInProzentNeu;}
	
	public int getSchildeInProzent() {return schildeInProzent;}
	
	public void setSchildeInProzent(int schildeInProzentNeu) {this.schildeInProzent = schildeInProzentNeu;}
	
	public int getHuelleInProzent() {return huelleInProzent;}
	
	public void setHuelleInProzent(int huelleInProzentNeu) {this.huelleInProzent = huelleInProzentNeu;}
	
	public int getLebenserhaltungssystemeInProzent() {return lebenserhaltungssystemeInProzent;}
	
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;}
	
	public int getAndroidenAnzahl() {return androidenAnzahl;}
	
	public void setAndroidenAnzahl(int androidenAnzahl) {this.androidenAnzahl = androidenAnzahl;}
	
	public String getSchiffsname() {return schiffsname;}
	
	public void setSchiffsname(String schiffsname) {this.schiffsname = schiffsname;}

	//Methoden
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public void photonentorpedoSchiessen(int AnzahlEinzusetzen, Raumschiff r) {
		if (AnzahlEinzusetzen > photonentorpedoAnzahl) {
			AnzahlEinzusetzen = photonentorpedoAnzahl;
		}
		
		if (photonentorpedoAnzahl == 0) {
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");
		} else {
			photonentorpedoAnzahl = photonentorpedoAnzahl--;
			nachrichtAnAlle(schiffsname + ": Photonentorpedo abgeschossen");
			treffer(r);
		}
	}
	
	public void phaserkanoneSchiessen(Raumschiff r) {
		if (energieversorgungInProzent < 50) {
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle(schiffsname + ": Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	
	private void treffer(Raumschiff r) {
		schildeInProzent = schildeInProzent - 50;
		if (schildeInProzent <= 0) {
			huelleInProzent = huelleInProzent - 50;
			energieversorgungInProzent = energieversorgungInProzent - 50;
		}
		if (huelleInProzent <= 0) {
			lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle(schiffsname + ": Lebenserhaltungssysteme vollständig zerstört");
		}
	}
	
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message + "\n");
	}
	
	static public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		System.out.printf("Alle LogBucheinträge:\n");
		return broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int photonentorpedoAnzahl) {
		
//		Ansonsten wird die Ladungsmenge Photonentorpedo über die Setter Methode vermindert und die Anzahl der Photonentorpedo im Raumschiff erhöht.
//		Konnten Photonentorpedos eingesetzt werden, so wird die Meldung [X] Photonentorpedo(s) eingesetzt auf der Konsole ausgegeben. [X] durch die Anzahl ersetzen.

		if (photonentorpedoAnzahl == 0) {
			System.out.printf(schiffsname + ": Keine Photonentorpedos gefunden!");
			nachrichtAnAlle(schiffsname + ": -=*Click*=-");
		}
	}
	
	public void reparaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlÜbergebenAndroiden) {
		double randomZahl = Math.random() * 100 ;
		int anzahlSchiffsstrukturen = 0;
		if (anzahlÜbergebenAndroiden > androidenAnzahl) {
			anzahlÜbergebenAndroiden = androidenAnzahl;
		}
		if (schutzschilde == true) {anzahlSchiffsstrukturen = anzahlSchiffsstrukturen++;}
		if (energieversorgung == true) {anzahlSchiffsstrukturen = anzahlSchiffsstrukturen++;}
		if (schiffshuelle == true) {anzahlSchiffsstrukturen = anzahlSchiffsstrukturen++;}
		
		double reparatur = randomZahl * anzahlÜbergebenAndroiden / anzahlSchiffsstrukturen;
		
		if (schutzschilde == true) {schildeInProzent = schildeInProzent+reparatur;}
		if (energieversorgung == true) {energieversorgungInProzent = energieversorgungInProzent+reparatur;}
		if (schiffshuelle == true) {huelleInProzent = huelleInProzent+reparatur;}
	}
	
	public void zustandRaumschiff() {
		System.out.println("Zustand des Schiffs: " + schiffsname);
		System.out.println("photonentorpedoAnzahl: " + photonentorpedoAnzahl);
		System.out.println("energieversorgungInProzent: " + energieversorgungInProzent + "%");
		System.out.println("schildeInProzent: " + schildeInProzent + "%");
		System.out.println("huelleInProzent: " + huelleInProzent + "%");
		System.out.println("lebenserhaltungssystemeInProzent: " + lebenserhaltungssystemeInProzent + "%");
		System.out.println("androidenAnzahl: " + androidenAnzahl);
	}
	
	public void ladungsverzeichnisAusgeben() {
		int count = 0;
		for (Ladung item : ladungsverzeichnis) {
			count++;
			System.out.printf("Ladung %d\n", count);
			System.out.printf("Bezeichnung: %s\n",item.getBezeichnung());
			System.out.printf("Menge: %d\n",item.getMenge());
		}
	}
	
	public void ladungsverzeichnisAufraeumen() {
		for (Ladung item : ladungsverzeichnis) {
			if (item.getMenge() == 0) {
				ladungsverzeichnis.remove(item);
			}
		}
	}
	

}
