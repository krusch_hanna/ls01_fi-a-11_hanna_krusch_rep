
public class TestStarTrek {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Objekte anlegen
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var");
		
		//Schiffe mit Ladung bef�llen
		klingonen.addLadung(new Ladung("Ferengi Schneckensaft", 200));
		klingonen.addLadung(new Ladung("Bat'leth Klingonen Schwert", 200));
		romulaner.addLadung(new Ladung("Borg-Schrott", 5));
		romulaner.addLadung(new Ladung("Rote Materie", 2));
		romulaner.addLadung(new Ladung("Plasma-Waffe", 50));
		vulkanier.addLadung(new Ladung("Forschungssonde", 35));
		vulkanier.addLadung(new Ladung("Photonentorpedo", 3));
		
		//Ausgabe Tests
		klingonen.photonentorpedoSchiessen(1, romulaner);
		romulaner.phaserkanoneSchiessen(vulkanier);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		System.out.println("");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("");
		klingonen.photonentorpedoSchiessen(2, romulaner);
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println("");
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println("");
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		System.out.println("");
		System.out.println(Raumschiff.eintraegeLogbuchZurueckgeben());
	}

}
