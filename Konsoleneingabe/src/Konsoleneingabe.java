import java.util.Scanner;
public class Konsoleneingabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Deklarationen
		//String name; 
		//int alter;
		//float koerpergroesse;
		//char geschlecht;
		
		char anfangsbuchstabe;
		String wohnort;
		int hausnr;
		
		
		//Scanner �ffnen
		Scanner myScanner = new Scanner(System.in); 
		
		//Name
		//System.out.print("Geben Sie ihren Namen ein: ");
		//name = myScanner.next();
		//System.out.println(name);
		
		//Alter
		//System.out.print("Geben Sie ihr Alter ein: ");
		//alter = myScanner.nextInt();
		//System.out.println(alter);
		
		//K�rpergr��e
		//System.out.print("Geben Sie ihre K�rpergr��e ein: ");
		//koerpergroesse = myScanner.nextFloat();
		//System.out.println(koerpergroesse);
		
		//Geschlecht
		//System.out.print("Geben Sie ihr Geschlecht (w/m/d) ein: ");
		//geschlecht = myScanner.next().charAt(0);
		//System.out.println(geschlecht);
		
		//Anfangsbuchstabe
		System.out.print("Bitte den Anfangsbuchstaben eingeben: ");
		anfangsbuchstabe = myScanner.next().charAt(0);
		
		//Wohnort
		System.out.print("Bitte den Wohnort ein: ");
		wohnort = myScanner.next();
		
		//Hausnummer
		System.out.print("Bitte Sie die Hausnummer ein: ");
		hausnr = myScanner.nextInt();
		
		//Ausgabe
		System.out.println(" ");
		System.out.println("Ihr Anfangsbuchstabe ist: " + anfangsbuchstabe);
		System.out.println("Sie wohnen in: " + wohnort);
		System.out.println("Ihr Hausnummer ist: " + hausnr);
		
		//Scanner schlie�en
		myScanner.close();
		
		
		
		
	}

}
