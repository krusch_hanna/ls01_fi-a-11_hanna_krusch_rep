import java.util.Scanner;
public class WhileZaehlen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		System.out.print("F�r heraufz�hlen geben Sie die 1 ein. \nF�r herunterz�hlen geben Sie die 2 ein. \n");
   	    byte variante = tastatur.nextByte();
   	    int i;
   	   
   	    switch (variante) {
   	    case 1 :
   	    	System.out.print("Bis wohin soll gez�hlt werden? \nBis");
   	    	int grenze = tastatur.nextInt();
   	    	i = 1;
   	    	while (i <= grenze) {
   				System.out.println(i);
   				i++;
   			}
   		    break;
	   
   	    case 2 :
   	    	System.out.print("Von wo soll runtergez�hlt werden? \nVon");
   	    	grenze = tastatur.nextInt();
   	    	i = grenze;
   	    	while (i >= 1) {
   				System.out.println(i);
   				i--;
   			}
   		    break;
	   
	    default:
		    System.out.println("Bitte nur 1 oder 2 eingeben!");
		    break;
   	    }
   	    tastatur.close();
   	   	
	}

}
