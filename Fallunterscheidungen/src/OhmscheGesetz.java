import java.util.Scanner;

public class OhmscheGesetz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Variablen deklariert
		Scanner myScanner = new Scanner(System.in);
		float U;
		float I;
		float R;
		
		//Eingabe der zu berechnenden Gr��e
		System.out.printf("Welche elektrische Kenngr��e soll berechnet werden? (U, I, R): ");
		char zuBerechnen = myScanner.next().charAt(0);
		
		//switch-case
		switch (zuBerechnen)
		{
			case 'U':
				System.out.printf("Bitte I, den Strom, in Ampere eingeben: ");
				I = myScanner.nextFloat();
				System.out.printf("Bitte R, den Widerstand, in Ohm eingeben: ");
				R = myScanner.nextFloat();
				
				System.out.println("\nU = R * I");
				System.out.println("U = " + R * I + " Volt");
				break;
			
			case 'I':
				System.out.printf("Bitte U, die Spannung, in Volt eingeben: ");
				U = myScanner.nextFloat();
				System.out.printf("Bitte R, den Widerstand, in Ohm eingeben: ");
				R = myScanner.nextFloat();
				
				System.out.println("\nI = U / R");
				System.out.println("I = " + U / R + " Ampere");
				break;
				
			case 'R':
				System.out.printf("Bitte I, den Strom, in Ampere eingeben: ");
				I = myScanner.nextFloat();
				System.out.printf("Bitte U, die Spannung, in Volt eingeben: ");
				U = myScanner.nextFloat();
				
				System.out.println("\nR = U / I");
				System.out.println("R = " + U / I + " Ohm");
				break;
				
			default:
				System.out.println("Sie haben keine von den vorher genannten elektrischen Gr��en ausgew�hlt!");
		}
		myScanner.close();
	}
}
