import java.util.Scanner;

public class Sortieren {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Variablen deklariert
		Scanner myScanner = new Scanner(System.in);
		int temp;
		
		System.out.println("Bitte immer nur ein Zeichen, also auch nur einstellige Zahlen.");
		int asciieins = Eingabe(myScanner);
		int asciizwei = Eingabe(myScanner);
		int asciidrei = Eingabe(myScanner);
		
		//Sortierungsvorgang über ASCII-Tabelle
		if (asciieins > asciizwei)
		{
			temp = asciizwei;
			asciizwei = asciieins;
			asciieins = temp;
		}
		
		if (asciieins > asciidrei)
		{
			temp = asciidrei;
			asciidrei = asciieins;
			asciieins = temp;
		}
		
		if (asciizwei > asciidrei)
		{
			temp = asciidrei;
			asciidrei = asciizwei;
			asciizwei = temp;
		}
		
		
		//Ausgabe der Zeichen und Umwandeln der ASCII-Ziffern in ursprünglichen Zeichen
		System.out.println(" ");
		System.out.println(Umwandeln(asciieins));
		System.out.println(Umwandeln(asciizwei));
		System.out.println(Umwandeln(asciidrei));
		
	}
	
	//Methode der Eingabe der Zeichen
	public static int Eingabe(Scanner myScanner)
	{
		System.out.printf("Bitte ein Zeichen eingeben: ");
		char Zeichen = myScanner.next().charAt(0);
		int ascii = Zeichen;
		return ascii;
	}
	
	//Umwandeln der ASCII-Ziffern in ursprüngliche Zeichen
	public static char[] Umwandeln(int asciiZahl)
	{
		char[] ersteAusgabe = Character.toChars(asciiZahl);
		return ersteAusgabe;
	}

}
